﻿using AL.Import.ShipmentSaving.Helpers;
using Cormant.Dce;
using Cormant.Dce.Common;
using Cormant.Dce.Common.Entities.UserDefinedFields;
using Cormant.Dce.Rules.Data;
using Cormant.Dce.Rules.Extension;
using Cormant.Dce.Shipments.Entities;
using Cormant.Dce.Shipments.Enums;
using Cormant.Dce.Shipments.Manager;
using Cormant.Dce.Shipments.Service.Auditing;
using Cormant.Dce.Shipments.Service.Queues;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace AL.Import.ShipmentSaving
{
    public class SavingCoreExtensionLibrary : IExtension<ExtensionParams>
    {
        #region FIELDS
        protected static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private const string CATEGORY_DM = "DM";
        private const string CATEGORY_LVD = "LVD";
        private const string CATEGORY_FRML = "FRML";
        private const string CATEGORY_RESTRICT = "RESTRICT";

        private const string QUEUE_CLEARANCE_SUPPORT = "Clearance Support";
        private const string QUEUE_ENTRY_RELEASED = "Entry Released";
        private const string QUEUE_PMIS = "Pmis";
        private const string QUEUE_BROKER_REVIEW = "Broker Review";
        private const string QUEUE_SPECIAL_HANDLING = "Special Handling";
        private const string QUEUE_REJECTED = "Rejected Imports";

        private const string ENTRY_TYPE_CLEAR = "Clear";
        private const string STATUS_COND = "COND";
        private const string STATUS_RLSE = "RLSE";
        private const string STATUS_PMIS = "PMIS";
        private const string STATUS_BRKR = "BRKR";
        private const string STATUS_CNPK = "CNPK";
        private const string STATUS_DIP = "DIP";
        private const string STATUS_DOD = "DOD";
        private const string STATUS_DUTY = "DUTY";
        private const string STATUS_FRML = "FRML";
        private const string STATUS_MPWI = "MPWI";
        private const string STATUS_NCI = "NCI";
        private const string STATUS_VALU = "VALU";
        private const string STATUS_XMPT = "XMPT";
        private const string STATUS_EXAM = "EXAM";
        private const string STATUS_RTO = "RTO";

        private const string CALCULATED_VARIABLE_SHIPMENTVALUE = "SHIPMENTVALUE";
        private const string CALCULATED_VARIABLE_ITEMVALUE = "ITEMVALUE";

        private const string INCOTERM_XXX = "XXX";
        private const string INCOTERM_FAS = "FAS";
        private const string INCOTERM_DAF = "DAF";
        private const string INCOTERM_DES = "DES";
        private const string INCOTERM_DEQ = "DEQ";
        private const string INCOTERM_DDU = "DDU";
        private const string INCOTERM_FOB = "FOB";
        private const string INCOTERM_DAP = "DAP";
        private const string INCOTERM_EXW = "EXW";        

        protected const string MandatoryErrorMessage = "There are missing mandatory fields";
        protected const string ErrorMessageInExecuting =
               "An error was encountered while trying to execute {0} method in Saving CEL for shipment {1}|{2}:  {3}";
        protected const string SavingFrmlShipmentWithBlankHsCode =
            "HS Code of LineItem {0} is blank.";
        protected const string ErrorAutoSubmitRestrictedShipment = "RESTRICT shipments cannot be submitted to Customs.";
        protected const string BlankQtyErrorMessage =
            "Line Item #{0}: QTY field cannot be blank or 0.00 if a shipment has an associated UOM/Supplementary Unit.";
        protected const string HistoryAutoSubmissionFailed =
            "{0} was not submitted on change of Goods Declaration Number. Reason: {1} \nReverted Goods Declaration Number.";

        protected SendToQueueParamsExtension SendShipmentToQueue;
        #endregion

        #region PUBLIC METHODS
        public void Execute(ExtensionParams extensionParams)
        {
            if (extensionParams.Process == Processes.Loading)
                ExecuteLoadingRules(extensionParams);

            else if (extensionParams.Process == Processes.Saving)
                ExecuteSavingRules(extensionParams);        
        } 
        #endregion

        #region PRIVATE METHODS        

        private void DistributeGrossWeightToLineItems(Shipment shipment, IList<LineItem> lineItems)
        {
            try
            {
                var shipmentValueVariable = shipment.CalculatedVariables.FirstOrDefault(item => item.Name == CALCULATED_VARIABLE_SHIPMENTVALUE);
                if (shipmentValueVariable != null)
                {
                    var shipmentValue = shipmentValueVariable.CalculatedValue;
                    foreach (var lineItem in lineItems)
                    {
                        var itemValueVariable = lineItem.CalculatedVariables.FirstOrDefault(item => item.Name == CALCULATED_VARIABLE_ITEMVALUE);
                        if (itemValueVariable != null)
                        {
                            var itemValue = itemValueVariable.CalculatedValue;
                            if (itemValue == (decimal)0.00 && itemValueVariable.ManualValue.HasValue)
                                itemValue = itemValueVariable.ManualValue.Value;

                            var distributedGrsWtValue = RulesUtil.Round(itemValue / shipmentValue * shipment.Weight, 3);
                            if (distributedGrsWtValue == (decimal)0.00)
                                distributedGrsWtValue = (decimal)0.1;

                            UDFHelper.SetLineUdfValueNumeric(lineItem, "Grs Wt", distributedGrsWtValue);
                        }                        
                    }
                }                
            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    "DistributeLineGrossWeight",
                    "DistributeLineGrossWeight",
                    shipment.Id,
                    shipment.HawbNumber,
                    e);
            }
        }

        private void SetDefaultSupplierOfLineItems(Shipment shipment, IList<LineItem> lineItems)
        {
            try
            {
                var addresslines = RulesUtil.StringLength(shipment.Shipper.AddressLine1 + " " + shipment.Shipper.AddressLine2 + " " + shipment.Shipper.AddressLine3) > 255 ?
                        RulesUtil.Substring(shipment.Shipper.AddressLine1 + " " + shipment.Shipper.AddressLine2 + " " + shipment.Shipper.AddressLine3, 0, 255) :
                        shipment.Shipper.AddressLine1 + " " + shipment.Shipper.AddressLine2 + " " + shipment.Shipper.AddressLine3;

                foreach (var lineItem in lineItems)
                {
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_Name", shipment.Shipper.Name);
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_City", shipment.Shipper.City);
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_CountryCode", shipment.Shipper.CountryCode);
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_State", shipment.Shipper.CountryCode);
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_Add1", addresslines);
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_PostCode", shipment.Shipper.PostalCode);
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_Ctc", shipment.Shipper.ContactName);
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_Tel#", shipment.Shipper.TelephoneNumber);
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_Mob#", shipment.Shipper.MobileNumber);
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_Fax", shipment.Shipper.FaxNumber);
                    UDFHelper.SetLineUdfValueText(lineItem, "OtherParty4_Email", shipment.Shipper.EmailAddress); 
                }
            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "DefaultSupplier",
                    shipment.Id,
                    shipment.HawbNumber,
                    e);
            }
        }

        private void SetShipmentIncoTermToEXW(Shipment shipment)
        {
            try
            {
                if (Equals(shipment.IncoTerms, INCOTERM_XXX))
                    shipment.IncoTerms = INCOTERM_EXW;
            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "Incoterm EXW",
                    shipment.Id,
                    shipment.HawbNumber,
                    e);
            }
        }

        private void SetShipmentIncoTermToDAP(Shipment shipment)
        {
            try
            {
                if (Equals(shipment.IncoTerms, INCOTERM_DDU))
                    shipment.IncoTerms = INCOTERM_DAP;
            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "Incoterm DAP",
                    shipment.Id,
                    shipment.HawbNumber,
                    e);
            }
        }

        private void SetShipmentIncoTermToFOB(Shipment shipment)
        {
            try
            {
                if (Equals(shipment.IncoTerms, INCOTERM_FAS) || Equals(shipment.IncoTerms, INCOTERM_DAF) ||
                    Equals(shipment.IncoTerms, INCOTERM_DES) || Equals(shipment.IncoTerms, INCOTERM_DEQ))
                {
                    shipment.IncoTerms = INCOTERM_FOB;
                }
            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "Incoterm FOB",
                    shipment.Id,
                    shipment.HawbNumber,
                    e);
            }
        }

        private void SendToDupSplitQueue(ExtensionParams extensionParams)
        {
            try
            {
                if ((extensionParams.Shipment.Status.IsDuplicate || extensionParams.Shipment.Status.IsSplitShipment) &&
                    !extensionParams.Shipment.Status.IsUpdateRequired)
                {
                    SendShipmentToQueue.SendToQueue("Dup/Split");
                }
            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "Dup/Split Queue",
                    extensionParams.Shipment.Id,
                    extensionParams.Shipment.HawbNumber,
                    e);
            }
        }

        private void SendToPNRQueueIfCategoryIsRestrict(ExtensionParams extensionParams)
        {
            try
            {
                if(Equals(extensionParams.Shipment.Status.ClearanceCategory.Code, CATEGORY_RESTRICT))
                    SendShipmentToQueue.SendToQueue("PNR");

            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "PNRShipments",
                    extensionParams.Shipment.Id,
                    extensionParams.Shipment.HawbNumber,
                    e);
            }
        }

        private void SendToRejectedQueueIfStatusIsRej(ExtensionParams extensionParams)
        {
            try
            {
                if (extensionParams.IgnoredFields.Contains(StandardField.ShipmentAcaStatusCode) &&
                    Equals(extensionParams.Shipment.Status.AcaStatusCode, "REJ"))
                    SendShipmentToQueue.SendToQueue(QUEUE_REJECTED);

            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "RejectedShipmentsOnSave",
                    extensionParams.Shipment.Id,
                    extensionParams.Shipment.HawbNumber,
                    e);
            }
        }

        private void SetManifestedPiecesIfPreAlertPiecesIsZeroAndIsNotManuallySet(ExtensionParams extensionParams)
        {
            try
            {
                var shipment = extensionParams.Shipment;

                if (shipment.PreAlertPieces == 0 && !extensionParams.IgnoredFields.Contains(StandardField.ShipmentPreAlertPieces))
                    shipment.PreAlertPieces = shipment.ShipmentDetailPieces;
            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "ManifestedPieces",
                    extensionParams.Shipment.Id,
                    extensionParams.Shipment.HawbNumber,
                    e);
            }
        }

        private void CheckForMissingMandatoryField(ExtensionParams extensionParams)
        {
            try
            {
                if (extensionParams.MissingMandatoryFields.ShipmentFields.Any() ||
                     extensionParams.MissingMandatoryFields.LineItemFields.Any())
                {
                    extensionParams.Shipment.Status.IsUpdateRequired = true;
                    SendShipmentToQueue.SendToQueue(QueueType.ClearanceSupport, MandatoryErrorMessage);
                }
            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "HasMissingMandatoryField",
                    extensionParams.Shipment.Id,
                    extensionParams.Shipment.HawbNumber,
                    e);
            }
        }

        private void SetReadyForEntryIfShipmentStatusIsCond(ExtensionParams extensionParams)
        {
            if (extensionParams.Shipment.Status.IsUpdateRequired)
                return;

            if (extensionParams.Shipment.Status != null)
            {
                extensionParams.Shipment.Status.IsReadyForEntry = false;
                if (Equals(extensionParams.Shipment.Status.AcaStatusCode, STATUS_COND))
                    extensionParams.Shipment.Status.IsReadyForEntry = true;
            }
        }

        private void ExecuteSavingRules(ExtensionParams extensionParams)
        {
            var shipment = extensionParams.Shipment;
            var lineItems = shipment.LineItems;

            SetManifestedPiecesIfPreAlertPiecesIsZeroAndIsNotManuallySet(extensionParams);
            SendToPNRQueueIfCategoryIsRestrict(extensionParams);
            SendToRejectedQueueIfStatusIsRej(extensionParams);
            ProcessCustomerTypeCode(extensionParams);
            SetReadyForEntryIfShipmentStatusIsCond(extensionParams);
            SendShipmentToDesignatedQueue(extensionParams);

            // Execute line item rules
            DistributeGrossWeightToLineItems(shipment, lineItems);
            SetDefaultSupplierOfLineItems(shipment, lineItems);
        }

        private void SendShipmentToDesignatedQueue(ExtensionParams extensionParams)
        {
            if (extensionParams.Shipment.Status.IsUpdateRequired)
                return;

            var shipment = extensionParams.Shipment;
            var countryCode = shipment.CountryCode;
            var moduleFlag = shipment.ModuleFlag;
            if (extensionParams.Shipment.Status != null)
            {
                var shipmentStatus = extensionParams.Shipment.Status.AcaStatusCode;

                if (Equals(shipmentStatus, STATUS_BRKR) || Equals(shipmentStatus, STATUS_CNPK) || Equals(shipmentStatus, STATUS_DIP) ||
                    Equals(shipmentStatus, STATUS_DOD) || Equals(shipmentStatus, STATUS_DUTY) || Equals(shipmentStatus, STATUS_FRML) ||
                    Equals(shipmentStatus, STATUS_MPWI) || Equals(shipmentStatus, STATUS_NCI) || Equals(shipmentStatus, STATUS_VALU) ||
                    Equals(shipmentStatus, STATUS_XMPT))
                    SendToQueue(shipment, QUEUE_CLEARANCE_SUPPORT, countryCode, moduleFlag, extensionParams.Auditor);

                else if (Equals(shipmentStatus, STATUS_EXAM) || (Equals(shipmentStatus, STATUS_RTO)))
                    SendToQueue(shipment, QUEUE_SPECIAL_HANDLING, countryCode, moduleFlag, extensionParams.Auditor);

                else if (Equals(shipmentStatus, STATUS_RLSE))
                    SendToQueue(shipment, QUEUE_ENTRY_RELEASED, countryCode, moduleFlag, extensionParams.Auditor);

                else if (Equals(shipmentStatus, STATUS_COND))
                    SendToQueue(shipment, QUEUE_BROKER_REVIEW, countryCode, moduleFlag, extensionParams.Auditor);
            }
        }

        private void ExecuteLoadingRules(ExtensionParams extensionParams)
        {
            try
            {
                var shipment = extensionParams.Shipment;
                var countryCode = extensionParams.Shipment.CountryCode;
                var moduleFlag = extensionParams.Shipment.ModuleFlag;
                var lineItems = shipment.LineItems;

                CheckForMissingMandatoryField(extensionParams);
                SetManifestedPiecesIfPreAlertPiecesIsZeroAndIsNotManuallySet(extensionParams);
                SendToPNRQueueIfCategoryIsRestrict(extensionParams);
                SendToDupSplitQueue(extensionParams);
                ExecuteCaLoadingRules(extensionParams, shipment, countryCode, moduleFlag);
                ExecutePaLoadingRules(extensionParams, shipment, countryCode, moduleFlag);
                SetReadyForEntryIfShipmentStatusIsCond(extensionParams);
                SendShipmentToDesignatedQueue(extensionParams);
                SetShipmentIncoTermToEXW(shipment);
                SetShipmentIncoTermToFOB(shipment);
                SetShipmentIncoTermToDAP(shipment);

                // Execute line item rules
                DistributeGrossWeightToLineItems(shipment, lineItems);
                SetDefaultSupplierOfLineItems(shipment, lineItems);
            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "ExecuteLoadingRules",
                    extensionParams.Shipment.Id,
                    extensionParams.Shipment.HawbNumber,
                    e);
            }
        }

        private void ExecutePaLoadingRules(ExtensionParams extensionParams, Cormant.Dce.Shipments.Entities.Shipment shipment, string countryCode, ModuleFlag moduleFlag)
        {
            if (shipment.Movement == null || shipment.Status.IsUpdateRequired)
                return;

            // Shipment.ShipmentDetailPieces is SD pieces
            // Shipment.PreAlertPieces is Manifested pieces
            if (shipment.ShipmentDetailPieces != shipment.PreAlertPieces)
            {
                SetStatus(extensionParams, STATUS_PMIS);
                SendToQueue(shipment, QUEUE_PMIS, countryCode, moduleFlag, extensionParams.Auditor);
            }
        }

        private void ExecuteCaLoadingRules(ExtensionParams extensionParams, Cormant.Dce.Shipments.Entities.Shipment shipment, string countryCode, ModuleFlag moduleFlag)
        {
            if (shipment.Movement != null || shipment.Status.IsUpdateRequired)
                return;

            if (Equals(shipment.Status.ClearanceCategory.Code, CATEGORY_DM))
            {
                SetStatus(extensionParams, STATUS_COND);
                SetEntryType(ENTRY_TYPE_CLEAR, extensionParams);
            }

            else if (Equals(shipment.Status.ClearanceCategory.Code, CATEGORY_LVD))
                SetStatus(extensionParams, STATUS_COND);

            else if (Equals(shipment.Status.ClearanceCategory.Code, CATEGORY_FRML))
            {
                SetStatus(extensionParams, STATUS_FRML);                
                SendToQueue(shipment, QUEUE_CLEARANCE_SUPPORT, countryCode, moduleFlag, extensionParams.Auditor);
            }
        }

        private void ProcessCustomerTypeCode(ExtensionParams extensionParams)
        {
            try
            {
                var ShpPayerDHLAcctNoUDF = extensionParams.Shipment.EditedCustomFields
                       .SingleOrDefault(udf => Equals(udf.Label, "ShpPayerDHLAcctNo"));
                if (RulesUtil.In(extensionParams.Process, Processes.Loading, Processes.Saving) &&
                    !RulesUtil.IsBlank(ShpPayerDHLAcctNoUDF?.NewValueText) && ShpPayerDHLAcctNoUDF?.NewValueText == (GetRefDataExtension.GetRefData("IMPEXAccount",
                        ShpPayerDHLAcctNoUDF?.NewValueText,
                        UDFValueType.Value1, extensionParams)))
                {
                    var ShpAcctTypeUdf = extensionParams.Shipment.Shipper.EditedCustomFields.SingleOrDefault(udf => Equals(udf.Label, "CustomerTypeCode"));

                    ShpAcctTypeUdf.SetNewValue("COL");
                }

                if (RulesUtil.In(extensionParams.Process, Processes.Loading, Processes.Saving) &&
                    !RulesUtil.IsBlank(ShpPayerDHLAcctNoUDF?.NewValueText) && ShpPayerDHLAcctNoUDF?.NewValueText != (GetRefDataExtension.GetRefData("IMPEXAccount",
                        ShpPayerDHLAcctNoUDF?.NewValueText,
                        UDFValueType.Value1, extensionParams)))
                {
                    var ShpAcctTypeUdf =
                        extensionParams.Shipment.Shipper.EditedCustomFields.SingleOrDefault(
                            udf => Equals(udf.Label, "CustomerTypeCode"));

                    ShpAcctTypeUdf.SetNewValue("REG");
                }

            }
            catch (Exception e)
            {
                Logger.WarnFormat(
                    System.Globalization.CultureInfo.InvariantCulture,
                    ErrorMessageInExecuting,
                    "CustomerTypeCode",
                    extensionParams.Shipment.Id,
                    extensionParams.Shipment.HawbNumber,
                    e);
            }
        }

        private void SendToQueue(Cormant.Dce.Shipments.Entities.Shipment shipment,
            string queueName,
            string countryCode,
            ModuleFlag moduleFlag,
            ShipmentAuditor auditor)
        {
            var workQueue = new WorkQueueManager(countryCode, moduleFlag).GetByName(queueName);
            if (workQueue == null)
                auditor.AddMessage(string.Format(
                    "Unable to send shipment to {0} because it is invalid or inactive.",
                    queueName));
            else
                SendToQueue(shipment, workQueue, auditor);
        }

        private void SendToQueue(
            Cormant.Dce.Shipments.Entities.Shipment shipment,
            WorkQueue workQueue,
            ShipmentAuditor auditor)
        {
            var continueSend = true;
            var hasThresholdsRequiredActions = false;
            var requiredActionManager = new RequiredActionManager();
            var requiredActionController = new RequiredActionController(shipment, requiredActionManager);

            if (workQueue.QueueType == QueueType.ClearanceSupport)
            {
                //TODO: Make Cormant.Dce.Rules.Properties.Settings public to access SendToUpdateRequiredOnlyOnce setting
                var hasAlreadyBeenToCsForKeyFields = shipment.Status.IsUpdateRequired &&
                                                     shipment.Status.IsSentToCSForKeyFields;

                hasThresholdsRequiredActions =
                    requiredActionController.RequiredActions.Any(item =>
                        item.RequiredActionType == RequiredActionType.Threshold);

                var hasAlreadyBeenToCsForThresholds = hasThresholdsRequiredActions &&
                                                      shipment.Status.IsSentToCSForThresholds;

                var isInBrokerReview = shipment.Status.Queue != null &&
                                       shipment.Status.Queue.QueueType == QueueType.DeclarantReview;

                continueSend = !(hasAlreadyBeenToCsForKeyFields || hasAlreadyBeenToCsForThresholds || isInBrokerReview);
            }

            if (!continueSend) return;
            var queueState = QueueStateProvider.Create(workQueue,
                shipment.CountryCode,
                shipment.ModuleFlag,
                null, null, null,
                requiredActionController);

            queueState.Shipment = shipment;
            queueState.ApplyToShipment();

            auditor.Context.SendMessage = null;

            if (workQueue.QueueType != QueueType.ClearanceSupport) return;
            if (shipment.Status.IsUpdateRequired)
                shipment.Status.IsSentToCSForKeyFields = true;

            if (hasThresholdsRequiredActions)
                shipment.Status.IsSentToCSForThresholds = true;
        }

        private static void SetStatus(ExtensionParams extensionParams, string status)
        {
            if (RulesDataSet.ClearanceStatusControllerFactory == null)
                throw new InvalidOperationException(
                    "RulesDataSet.ClearanceStatusControllerFactory has not been initialized.");

            var controller = RulesDataSet.ClearanceStatusControllerFactory();
            controller.ValidateHoldStatus = false;

            var clearanceStatus =
                new ClearanceStatusManager(extensionParams.Shipment.CountryCode, extensionParams.Shipment.ModuleFlag)
                    .GetByStatus("DCE", "DCE", status).FirstOrDefault();

            if (clearanceStatus == null)
                extensionParams.Auditor.AddMessage($"Unable to set Status to {status} because it is invalid.");
            else
            {
                extensionParams.ClearanceStatusChangeResult = controller.ApplyStatus(
                    extensionParams.Shipment, clearanceStatus, extensionParams.Auditor.Context.User);

                if (extensionParams.ClearanceStatusChangeResult.StatusChanged)
                {
                    extensionParams.Shipment.Status.AcaStatusCode = status;
                    extensionParams.MakeClearanceStatusFile = true;
                }
            }
        }

        private void SetEntryType(string type, ExtensionParams extensionParams)
        {
            if (extensionParams.CanUpdateField(StandardField.EntryEntryType))
            {
                var entryTypeManager = new EntryTypeManager(
                    extensionParams.Shipment.CountryCode,
                    extensionParams.Shipment.ModuleFlag);
                EntryType entryType = entryTypeManager.FindByType(type);

                if (entryType != null)
                    extensionParams.Shipment.EntryType = entryType.Type;
            }
        }

        private bool Equals(string value1, string value2)
        {
            return string.Compare(value1, value2, StringComparison.OrdinalIgnoreCase) == 0;
        } 
        #endregion
    }
}
