﻿using Cormant.Dce.Common.Entities.UserDefinedFields;
using Cormant.Dce.Common.Managers.UserDefinedFields;
using Cormant.Dce.Rules.Extension;
using Cormant.Dce.Shipments.Enums;

namespace AL.Import.ShipmentSaving.Helpers
{
    public class GetRefDataExtension
    {
        #region Methods

        public static string GetRefData(string tableName,
            string value,
            UDFValueType valueType,
            ExtensionParams extensionParams)
        {
            var row = new InstanceLevelReferenceDataManager().GetByValueType(extensionParams.Shipment.CountryCode,
                extensionParams.Shipment.ModuleFlag, tableName,
                value, UDFValueType.DisplayName, true);

            return row != null ? GetValueFromType(row, valueType) : string.Empty;
        }

        public static string GetRefData(string tableName,
            string value,
            UDFValueType valueType,
            UDFValueType valueTypeToDisplay,
            ExtensionParams extensionParams)
        {
            var row = new InstanceLevelReferenceDataManager().GetByValueType(extensionParams.Shipment.CountryCode,
                extensionParams.Shipment.ModuleFlag, tableName,
                value, valueType, true);

            return row != null ? GetValueFromType(row, valueTypeToDisplay) : string.Empty;
        }

        private static string GetValueFromType(InstanceLevelReferenceData lookup, UDFValueType valueType)
        {
            if (lookup == null) return string.Empty;
            switch (valueType)
            {
                case UDFValueType.Description: return lookup.Description;
                case UDFValueType.Value1: return lookup.Value1;
                case UDFValueType.Value2: return lookup.Value2;
                case UDFValueType.Value3: return lookup.Value3;
                case UDFValueType.Value4: return lookup.Value4;
                case UDFValueType.Value5: return lookup.Value5;
                case UDFValueType.Value6: return lookup.Value6;
            }
            return string.Empty;
        }

        #endregion
    }
}