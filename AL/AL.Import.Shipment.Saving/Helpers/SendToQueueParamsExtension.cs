﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cormant.Dce;
using Cormant.Dce.Rules.Extension;
using Cormant.Dce.Shipments.Entities;
using Cormant.Dce.Shipments.Enums;
using Cormant.Dce.Shipments.Manager;
using Cormant.Dce.Shipments.Service.Auditing;
using Cormant.Dce.Shipments.Service.Queues;

namespace AL.Import.ShipmentSaving.Helpers
{
    public class SendToQueueParamsExtension
    {
        #region Fields

        private readonly ShipmentAuditor _auditor;

        private readonly Cormant.Dce.Shipments.Entities.Shipment _shipment;

        #endregion

        #region Constructors

        public SendToQueueParamsExtension(Cormant.Dce.Shipments.Entities.Shipment shipment, ShipmentAuditor auditor)
        {
            _shipment = shipment;
            _auditor = auditor;
        }

        #endregion

        #region Methods

        public bool SendToQueue(string queueName, string message = null)
        {
            var workQueue = new WorkQueueManager(_shipment.CountryCode, _shipment.ModuleFlag)
                .GetByName(queueName);

            if (workQueue == null)
            {
                _auditor.AddMessage("Unable to send shipment to {queueName} because it is invalid or inactive.");
            }
            else
                SendToQueue(_shipment, workQueue, message);

            return false;
        }


        public void SendToQueue(QueueType queueType, string message = null)
        {
            var workQueue = new WorkQueueManager(_shipment.CountryCode, _shipment.ModuleFlag)
                .GetQueuesByQueueType(queueType)
                .FirstOrDefault();

            if (workQueue == null)
                _auditor.AddMessage("Unable to send shipment to {queueType} because it is invalid or inactive.");
            else
            {
                SendToQueue(_shipment, workQueue, message);
            }
        }

        private void SendToQueue(Cormant.Dce.Shipments.Entities.Shipment shipment, WorkQueue workQueue, string message)
        {
            var continueSend = true;
            var hasThresholdsRequiredActions = false;
            var requiredActionController = new RequiredActionController(shipment, new RequiredActionManager());

            if (workQueue.QueueType == QueueType.ClearanceSupport)
            {
                //TODO: Make Cormant.Dce.Rules.Properties.Settings public to access SendToUpdateRequiredOnlyOnce setting
                var hasAlreadyBeenToCsForKeyFields = shipment.Status.IsUpdateRequired &&
                                                     shipment.Status.IsSentToCSForKeyFields;

                hasThresholdsRequiredActions =
                    requiredActionController.RequiredActions.Any(item =>
                        item.RequiredActionType == RequiredActionType.Threshold);

                var hasAlreadyBeenToCsForThresholds = hasThresholdsRequiredActions &&
                                                      shipment.Status.IsSentToCSForThresholds;

                var isInBrokerReview = shipment.Status.Queue != null &&
                                       shipment.Status.Queue.QueueType == QueueType.DeclarantReview;

                continueSend = !(hasAlreadyBeenToCsForKeyFields || hasAlreadyBeenToCsForThresholds || isInBrokerReview);
                //continueSend = !(isInBrokerReview);
            }

            if (!continueSend) return;
            var queueState = QueueStateProvider.Create(workQueue,
                shipment.CountryCode,
                shipment.ModuleFlag,
                null, null, null,
                requiredActionController);

            queueState.Shipment = shipment;
            queueState.ApplyToShipment();

            _auditor.Context.SendMessage = message;

            if (workQueue.QueueType != QueueType.ClearanceSupport) return;
            if (shipment.Status.IsUpdateRequired)
                shipment.Status.IsSentToCSForKeyFields = true;

            if (hasThresholdsRequiredActions)
                shipment.Status.IsSentToCSForThresholds = true;
        }

        public void SendToQueue(WorkQueue workQueue, string message, ExtensionParams extensionParams)
        {
            var continueSend = true;
            var hasThresholdsRequiredActions = false;

            if (workQueue.QueueType == QueueType.ClearanceSupport)
            {
                // TODO: Cormant.Dce.Rules.Properties.Settings.Default.SendToUpdateRequiredOnlyOnce
                // This setting must be set to public
                var hasAlreadyBeenToCsForKeyFields = extensionParams.Shipment.Status.IsUpdateRequired &&
                                                     extensionParams.Shipment.Status.IsSentToCSForKeyFields;

                hasThresholdsRequiredActions =
                    extensionParams.RequiredActionController.RequiredActions.Any(item =>
                        item.RequiredActionType == RequiredActionType.Threshold);

                var hasAlreadyBeenToCsForThresholds = hasThresholdsRequiredActions &&
                                                      extensionParams.Shipment.Status.IsSentToCSForThresholds;

                var isInBrokerReview = extensionParams.Shipment.Status.Queue != null &&
                                       extensionParams.Shipment.Status.Queue.QueueType == QueueType.DeclarantReview;

                continueSend = !(hasAlreadyBeenToCsForKeyFields || hasAlreadyBeenToCsForThresholds || isInBrokerReview);
            }

            if (continueSend)
            {
                var queueState = QueueStateProvider.Create(workQueue,
                    extensionParams.Shipment.CountryCode,
                    extensionParams.Shipment.ModuleFlag,
                    null, null, null,
                    extensionParams.RequiredActionController);

                queueState.Shipment = extensionParams.Shipment;
                queueState.ApplyToShipment();

                _auditor.Context.SendMessage = message;

                if (workQueue.QueueType == QueueType.ClearanceSupport)
                {
                    if (extensionParams.Shipment.Status.IsUpdateRequired)
                        extensionParams.Shipment.Status.IsSentToCSForKeyFields = true;

                    if (hasThresholdsRequiredActions)
                        extensionParams.Shipment.Status.IsSentToCSForThresholds = true;
                }
            }
        }

        public void SendToQueue(QueueType queueType, string message, ExtensionParams extensionParams)
        {
            var workQueue =
                new WorkQueueManager(extensionParams.Shipment.CountryCode, extensionParams.Shipment.ModuleFlag)
                    .GetQueuesByQueueType(queueType)
                    .FirstOrDefault();

            if (workQueue == null)
            {
                extensionParams.Auditor.AddMessage(
                    "Unable to send shipment to {queueType} because it is invalid or inactive.");
            }
            else
            {
                SendToQueue(workQueue, message, extensionParams);
            }
        }

        #endregion
    }
}
