﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cormant.Dce.Common.Managers.UserDefinedFields;
using Cormant.Dce.Rules.Extension;
using Cormant.Dce.Shipments.Enums;
using Cormant.Dce.Shipments.Entities;

namespace AL.Import.ShipmentSaving.Helpers
{
    public class UDFHelper
    {
        public static string GetUdfValue(Cormant.Dce.Shipments.Entities.Shipment shipment, string udfLabel)
        {
            var udf = shipment.EditedCustomFields.FirstOrDefault(item => item.Label == udfLabel);

            if (udf != null)
            {
                return udf.NewValueText;
            }

            return string.Empty;
        }

        public static decimal GetUdfValueNumeric(Cormant.Dce.Shipments.Entities.Shipment shipment, string udfLabel)
        {
            var udf = shipment.EditedCustomFields.FirstOrDefault(item => item.Label == udfLabel);
            var ret = (decimal)0;

            if (udf != null)
            {
                ret = (decimal)udf.NewValueNumeric;
            }

            return ret;
        }



        public static decimal GetUdfValueOfLineNumeric(LineItem lineitem, string udfLabel)
        {
            var udf = lineitem.EditedCustomFields.FirstOrDefault(item => item.Label == udfLabel);
            decimal ret = (decimal)0;

            if (udf != null)
            {
                ret = (decimal)udf.NewValueNumeric;
            }

            return ret;
        }


        public static string GetUdfValueOfLineText(LineItem lineitem, string udfLabel)
        {
            var udf = lineitem.EditedCustomFields.FirstOrDefault(item => item.Label == udfLabel);
            var ret = string.Empty;

            if (udf != null)
            {
                return udf.NewValueText;
            }

            return String.Empty;
        }


        public static void SetShipmentUdfValueText(Cormant.Dce.Shipments.Entities.Shipment shipment, string udfLabel, string valueToSet)
        {
            var udf = shipment.EditedCustomFields.FirstOrDefault(item => item.Label == udfLabel);

            if (udf != null)
            {
                udf.NewValueText = valueToSet;
            }

        }

        public static void SetShipmentUdfValueNumeric(Cormant.Dce.Shipments.Entities.Shipment shipment, string udfLabel, decimal valueToSet)
        {
            var udf = shipment.EditedCustomFields.FirstOrDefault(item => item.Label == udfLabel);

            if (udf != null)
            {
                udf.NewValueNumeric = valueToSet;
            }

        }

        public static void SetLineUdfValueText(LineItem lineItem, string udfLabel, string valueToSet)
        {
            var udf = lineItem.EditedCustomFields.FirstOrDefault(item => item.Label == udfLabel);

            if (udf != null)
            {
                udf.NewValueText = valueToSet;
            }

        }

        public static void SetLineUdfValueNumeric(LineItem lineItem, string udfLabel, decimal valueToSet)
        {
            var udf = lineItem.EditedCustomFields.FirstOrDefault(item => item.Label == udfLabel);

            if (udf != null)
            {
                udf.NewValueNumeric = valueToSet;
            }

        }


        public static string GetUDLValue(ExtensionParams extensionParams, string customFieldName, UDFValueType valueType, string valueToLook)
        {
            var udfvalue = string.Empty;
            var customListManager = new CustomListManager();

            var lookup = customListManager.FindFirstByDisplayName(extensionParams.Shipment.CountryCode,
                    extensionParams.Shipment.ModuleFlag, customFieldName, valueToLook);
            if (lookup != null)
            {
                switch (valueType)
                {
                    case UDFValueType.DisplayName:
                        udfvalue = lookup.DisplayName;
                        break;
                    case UDFValueType.Description:
                        udfvalue = lookup.Description;
                        break;
                    case UDFValueType.Value1:
                        udfvalue = lookup.Value1;
                        break;
                    case UDFValueType.Value2:
                        udfvalue = lookup.Value2;
                        break;
                    case UDFValueType.Value3:
                        udfvalue = lookup.Value3;
                        break;
                    case UDFValueType.Value4:
                        udfvalue = lookup.Value4;
                        break;
                    case UDFValueType.Value5:
                        udfvalue = lookup.Value5;
                        break;
                    case UDFValueType.Value6:
                        udfvalue = lookup.Value6;
                        break;
                }
            }

            var resultUdfValue = udfvalue ?? string.Empty;
            return resultUdfValue;
        }
    }
}
