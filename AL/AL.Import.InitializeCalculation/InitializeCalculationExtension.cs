﻿using Cormant.Dce.Calculations.Managers;
using Cormant.Dce.Common.TimeZone;
using Cormant.Dce.Rules.Extension;
using Cormant.Dce.Shipments.Entities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AL.Import.InitializeCalculation
{
    public class InitializeCalculationExtension : IExtension<ExtensionParams>
    {
        private static readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private const string SERVICE_AREA_CODE = "TIA";
        private const string UNION_EXCHANGE_RATE_UDF_LABEL = "Union Exchange Rate";

        public void Execute(ExtensionParams extensionParams)
        {
            AssignExchangeRateToUdf(
                extensionParams,
                UNION_EXCHANGE_RATE_UDF_LABEL,
                "EUR",
                "LEK");            
        }

        private void AssignExchangeRateToUdf(
            ExtensionParams extensionParams, 
            string udfLabel, 
            string fromCurrency, 
            string toCurrency)
        {
            try
            {
                var effectiveExchangeRate = GetExchangeRate(
                    extensionParams.Shipment.CountryCode, 
                    fromCurrency, 
                    toCurrency, 
                    DateTime.Now.ToClientTime(SERVICE_AREA_CODE));

                var udf = GetUdf(
                    extensionParams.Shipment.EditedCustomFields,
                    udfLabel,
                    extensionParams.Shipment.Id);

                udf.NewValueNumeric = effectiveExchangeRate == 0 ? (decimal)0.0 : 1 / effectiveExchangeRate;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }
        }

        private static ShipmentEditedCustomField GetUdf(IList<ShipmentEditedCustomField> shipmentEditedCustomFields, string udfLabel, long shipmentId)
        {
            var resultUdf = shipmentEditedCustomFields.FirstOrDefault(udf => udf.Label.Equals(udfLabel));
            if (resultUdf == null)
                throw new Exception(
                    string.Format(
                        "The {0} UDF does not exist in this shipment: {1}.",
                        udfLabel, shipmentId));

            return resultUdf;
        }

        private decimal GetExchangeRate(string countryCode, string fromCurrency, string toCurrency, DateTime effectiveDate)
        {
            var exchangeRateManager = new ExchangeRateManager(countryCode);
            var exchangeRate = exchangeRateManager
                .GetExchangeRate(fromCurrency, toCurrency, effectiveDate);

            // Throw an exception if the exchange rate does not exist in the Exchange Rate config.
            if (exchangeRate == null)
                throw new Exception(
                    string.Format(
                        "The exchange rate {0} to {1} does not exist in the Exchange Rate config.", fromCurrency, toCurrency));

            return (decimal)exchangeRate.ExchangeRate;
        }
    }
}
